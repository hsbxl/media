# A collection of media for hackers, makers and everyone.

## Preamble 
A curated collection of advised media by [HSBXL](https://hsbxl.be/) and friends.

## Content

- [Things to watch](#things-to-watch)
    - [Movies](#movies)
    - [Series](#series)
    - [Documentaries](#documentaries)
    - [Ted talks](#ted-talks)
- [Things to listen to](#things-to-list-to)
    - [Music](#music)
    - [Podcast](#podcasts)
- [Things to read](#things-to-read)
    - [Novels](#novels)
    - [Non-fiction](#non-fiction)

## Things to watch

### Movies

- Based on true facts of a hacker in the early CCC days: https://en.wikipedia.org/wiki/23_(film)
- [Hackers](https://en.wikipedia.org/wiki/Hackers_(film)) Old movie
- [Pi](https://en.wikipedia.org/wiki/Pi_(film)) Movie about mathemathics and numerology, classic
- [A scanner darkly](https://en.wikipedia.org/wiki/A_Scanner_Darkly_(film)) Richar Linklater directed based on novel of Philip K. Dick
- [Snowden](https://en.wikipedia.org/wiki/Snowden_(film)) Movie about Snowden
- [La Zona](https://en.wikipedia.org/wiki/La_Zona_(film)) Social class 
- [We live in public](https://en.wikipedia.org/wiki/We_Live_in_Public) Privacy 

### Series

- [Libreflix](https://libreflix.org/) Alternative netflix with mostly (but not only) Spanish / Portugese socially aware stuff 
- [Black Mirror](https://en.wikipedia.org/wiki/Black_Mirror) For all your dystopic needs
-  
### Youtube :hankey:
- [LiveOverflow](https://www.youtube.com/channel/UClcE-kVhqyiHCcjYwcpfj9w) LiveOverflow - a Berlin hacker explaining hacking.

### Documentaries

- [All Creatures Welcome](https://sandratrostel.de/projects/acw/#) A utopian image of society in the digital era.
    - [On media ccc](https://media.ccc.de/v/all_creatures_welcome)
- [Nothing to hide ](https://vimeo.com/nothingtohide) Documentary dealing with mass surveillance. Available in English, German, French, ... 
- [Zero Days 2016](https://www.imdb.com/title/tt5446858/) A documentary focused on the Stuxnet virus and the start of a new form of warfare.
- [Citizenfour](https://en.wikipedia.org/wiki/Citizenfour) Doc about how Snowden leaked 
- [The great hack](https://en.wikipedia.org/wiki/The_Great_Hack) Cambridge analytica facebook
- 

### Ted talks

- [Why privacy matters](https://www.ted.com/talks/glenn_greenwald_why_privacy_matters) Glenn Greenwald about privacy
- [How the NSA betrayed the public's trust](https://www.ted.com/talks/mikko_hypponen_how_the_nsa_betrayed_the_world_s_trust_time_to_act) Mikko Hypponen about NSA

## Things to listen to

### Music
- [SomaFM](https://somafm.com/)  Independent Internet-only radio station
    - Check out [DEF CON Radio](https://somafm.com/defcon/)
- [Big Giant Circles](https://biggiantcircles.bandcamp.com/) Jimmy Hinson producting music for video games and more. 
- [VIRTUAVERSE​.​OST](https://masterbootrecord.bandcamp.com/) Master Boot Record

### Podcasts

**English**

- [Security Now](https://www.grc.com/securitynow.htm) Is a weekly security column and podcast from Steve Gibson and Leo Laporte. 
- [You are not that smart](https://youarenotsosmart.com/podcast/) Psychology podcast
- [BSD now](https://www.bsdnow.tv/) Everything BSD 

**German**

- [Logbuch Netzpolitik](https://logbuch-netzpolitik.de/) Linus Neumann and Tim Pritlove speaking about current topics in network policy.

## Things to read
- [A digital declaration: Big Data as Surveillance Capitalism](https://opencuny.org/pnmarchive/files/2019/01/Zuboff-Digital-Declaration.pdf) Essay on big data and surveillance

### Novels
- [Neuromancer](https://en.wikipedia.org/wiki/Neuromancer) First book of the Neuromancer-Trilogie by William Gibson.
- [Brave New World](https://en.wikipedia.org/wiki/Brave_New_World) Aldous Huxley masterpiece about our future.
- [Nineteen Eighty-Four: A Novel](https://en.wikipedia.org/wiki/Nineteen_Eighty-Four) George Orwell masterpiece. A **must** read
- [Blackout - Tomorrow will be too late](https://en.wikipedia.org/wiki/Blackout_(Elsberg_novel) What happens when the Smart Grids 

### Non-fiction

- [Factfulness 2018](https://en.wikipedia.org/wiki/Factfulness:_Ten_Reasons_We%27re_Wrong_About_the_World_%E2%80%93_and_Why_Things_Are_Better_Than_You_Think) Factfulness: Ten Reasons We're Wrong About the World--and Why Things Are Better Than You Think by Hans Rosling
- [The Age of Surveillance Capitalism](https://www.theguardian.com/books/2019/feb/02/age-of-surveillance-capitalism-shoshana-zuboff-review) Shoshana Zuboff
- [Sapiens: A Brief History of Humankind](https://en.wikipedia.org/wiki/Sapiens:_A_Brief_History_of_Humankind) Yuval Noah Harari 
    - Also checkout the follow-ups [Homo Deus: A Brief History of Tomorrow](https://en.wikipedia.org/wiki/Homo_Deus:_A_Brief_History_of_Tomorrow), [21 Lessons for the 21st Century](https://en.wikipedia.org/wiki/21_Lessons_for_the_21st_Century) 

### Blogs
- [Filippo.io](https://blog.filippo.io/) Filippo Valsorda - Cryptography and Go. 

